# Training and Evaluation

## Instructions & Rules

- Make sure the code you write works optimally. How you name variables, comment, and follow conventions will be considered during evaluation.
- You are free to use the internet. Mutual consultations or asking a colleague would be considered cheating. There is an exception: you can ask someone other than the traineess if English doesn't work for you, and translation engines haven't been helpful enough.
- All trainees' assignments are in their own branches. Pull your branch, work on the questions and submit a merge request for evaluation.
- All the theoretical questions are Base64 encoded. (Yes, I know it is tedious work.)
- Keep your code in solution.js, and any testing functions in index.js. You can use external dependencies, although it would mean losing points if the dependency use is deemed unnecessary.
- Türkçe cevaplayabilirsiniz. Cevapların size ait olması ve ne yazdığınızı bildiğinizi göstermesi doğruluğu kadar önemli. 
- Branch listesini görüyorsunuz, ister forklayıp çalışıp PR açın, ister kendi branchinizi clone'layarak başlayın. Tek branch çekmek için: ```git clone https://gitlab.com/adilumer/training-and-evaluation -b [branch-adı] --single-branch```

### Bol şans!